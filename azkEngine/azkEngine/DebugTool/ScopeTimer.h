#include <chrono>
#include <iostream>
#include <string>


class ScopeTimer
{
public:
	ScopeTimer(const char* _name);
	~ScopeTimer();

private:
	using chrono = std::chrono::system_clock;
	chrono::time_point m_StatrtTime;
	chrono::time_point m_EndTime;
	std::string m_Name;


};


