#include "ScopeTimer.h"
#include "..\Utility\Utility.h"



ScopeTimer::ScopeTimer(const char* _name)
	: m_Name(_name)
{
	m_StatrtTime = chrono::now();
}


ScopeTimer::~ScopeTimer()
{
	using namespace std::chrono;
	m_EndTime = chrono::now();
	auto dur = m_EndTime - m_StatrtTime;
	float sec = static_cast<float>(duration_cast<std::chrono::milliseconds>(dur).count() / 1000.f);
	MyOutputDebugString("%s : %f", m_Name.c_str(), sec);
}
