/**
* @file   Singleton.h
* @brief  Singletonクラスのヘッダファイル
* @author kotani
*/
#ifndef SINGLETON_H
#define SINGLETON_H


namespace azk
{
	/**
	* シングルトンクラスを使う際にこいつを使う
	*/
	template <class T>
	class Singleton {
	public:
		typedef T InstanceType;
		/**
		* インスタンスを取得する
		* @return インスタンス
		*/
		inline static InstanceType& Instance()
		{
			return *m_Instance;
		}

		/**
		* インスタンスを生成する
		*/
		inline static void Create()
		{
			if (m_Instance == nullptr)
			{
				m_Instance = new InstanceType;
			}
		}

		/**
		* インスタンスを破棄する
		*/
		inline static void Delete()
		{
			delete m_Instance;
			m_Instance = nullptr;
		}

	private:
		static T* m_Instance;

	};

	template <class Type>
	typename Singleton<Type>::InstanceType* Singleton<Type>::m_Instance = nullptr;
}


#define	SINGLETON_CREATE(InstanceType)	 azk::Singleton<InstanceType>::Create()
#define	SINGLETON_DELETE(InstanceType)	 azk::Singleton<InstanceType>::Delete()
#define	SINGLETON_INSTANCE(InstanceType) azk::Singleton<InstanceType>::Instance()

#endif

