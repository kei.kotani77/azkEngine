﻿/**
 * @file   Window.h
 * @brief  Windowクラスのヘッダファイル
 * @author kotani
 */
#ifndef WINDOW_H
#define WINDOW_H
#include <windows.h>
#include "..\Singleton\Singleton.h"

namespace azk
{
	class Window
	{
		friend Singleton<Window>;
	public:
		inline HWND GetWindowHandle() const
		{
			return m_hWnd;
		}

		inline HINSTANCE GetInstanceHandle() const
		{
			return m_hInstance;
		}

		inline RECT GetWindowSize()
		{
			return m_WindowSize;
		}

		/**
		* ウィンドウの生成
		* @param[in] _width ウィンドウの横幅
		* @param[in] _height ウィンドウの縦幅
		* @param[in] _windowName ウィンドウの名前
		* @return 成功したらS_OK
		*/
		HRESULT DispWindow(INT _width, INT _height, LPCTSTR _windowName);

		/**
		* ウィンドウの生成(アイコンあり)
		* @param[in] _width ウィンドウの横幅
		* @param[in] _height ウィンドウの縦幅
		* @param[in] _windowName ウィンドウの名前
		* @param[in] _iconName アイコンのパス
		* @return 成功したらS_OK
		*/
		HRESULT DispWindow(INT _width, INT _height, LPCTSTR _windowName, LPCTSTR _iconName);

	private:
		Window() :
			m_hInstance(nullptr),
			m_hWnd(nullptr)
		{
			m_hInstance = GetModuleHandle(nullptr);
		};
		~Window() {};
		RECT	m_WindowSize;
		HINSTANCE m_hInstance;
		HWND m_hWnd;

	};
}


#endif
