#ifndef APPLICATION_BASE_H
#define APPLICATION_BASE_H

#include <windows.h>
#include <string>
#include "..\Singleton/Singleton.h"

namespace azk
{
	struct SystemSetting
	{
		int			RefrashRate;
		int			WindowWidth;
		int			WindowHeight;
	};

	class ApplicationBase
	{
	public:

		ApplicationBase();
		virtual ~ApplicationBase();

		virtual bool Initialize() { return true; }

		virtual void Finalize() {}

		int Boot(const SystemSetting& _systemSetting, const char* _appName);

	private:

	};


}

#define APP_APPLICATION_CLASS(_class) \
class _class : public azk::IApplication

#define ENTRY_POINT() \
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR _cmd, INT)

#define APP_SETUP(_app, _appName) \
azk::IApplication::Instance().Boot(_setting, _appName)


#endif
