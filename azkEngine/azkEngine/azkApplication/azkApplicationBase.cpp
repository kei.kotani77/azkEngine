#include "azkApplicationBase.h"
#include "..\azkSceneManager\azkSceneManager.h"
#include "..\azkWindow\azkWindow.h"
#include <Windows.h>




namespace azk
{
	ApplicationBase::ApplicationBase()
	{
	}


	ApplicationBase::~ApplicationBase()
	{
	}

	int ApplicationBase::Boot(const SystemSetting& _systemSetting, const char* _appName)
	{
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
		SINGLETON_CREATE(SceneManager);
		SINGLETON_CREATE(Window);
		SINGLETON_INSTANCE(Window).DispWindow(
			_systemSetting.WindowWidth,
			_systemSetting.WindowHeight,
			_appName);

		if (!Initialize()) return -1;

		MSG Msg;
		ZeroMemory(&Msg, sizeof(Msg));
		while (Msg.message != WM_QUIT)
		{

			if (PeekMessage(&Msg, nullptr, 0U, 0U, PM_REMOVE))
			{
				TranslateMessage(&Msg);
				DispatchMessage(&Msg);
			}
			else
			{
				if (SINGLETON_INSTANCE(SceneManager).Run())
				{
					break;
				}
			}
		}
		Finalize();
		SINGLETON_DELETE(SceneManager);
		SINGLETON_DELETE(Window);
		return 0;
	}

}
