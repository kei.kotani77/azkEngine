﻿#ifndef UNIQUE_PTR_PRIVATE_H
#define UNIQUE_PTR_PRIVATE_H

template<typename Type>
UniquePtr<Type>::UniquePtr(Type* _type) 
	: SmartPtr<Type>(_type)
{
}

template<typename Type>
UniquePtr<Type>::UniquePtr()
{
	this->m_pInstance = nullptr;
}

template<typename Type>
UniquePtr<Type>::~UniquePtr()
{
	if (this->m_pInstance != nullptr)
	{
		(*this->m_pDeleter)(this->m_pInstance);
	}
	this->m_pInstance = nullptr;
}

template<typename Type>
void UniquePtr<Type>::Reset()
{
	if (this->m_pInstance != nullptr)
	{
		(*this->m_pDeleter)(this->m_pInstance);
	}
	this->m_pInstance = nullptr;
}

template<typename Type>
void UniquePtr<Type>::Reset(Type* _type)
{
	if (this->m_pInstance != nullptr)
	{
		(*this->m_pDeleter)(this->m_pInstance);
		delete m_pDeleter;
		m_pDeleter = nullptr;
	}
	this->m_pDeleter = new DeleterImpl<Type>();
	this->m_pInstance = nullptr;
	this->m_pInstance = _type;
}

template<typename Type>
Type* UniquePtr<Type>::operator->() const
{
	return this->m_pInstance;
}



#endif
