﻿/**
 * @file   SceneManager.h
 * @brief  SceneManagerクラスのヘッダファイル(未実装)
 * @author kotani
 */
#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H
#include <map>
#include <string>
#include "..\Singleton\Singleton.h"
#include "..\SmartPointer\SharedPtr.h"

namespace azk
{
	class SceneBase;

	/**
	 * ゲームのシーン遷移を管理するクラス
	 */
	class SceneManager : public Singleton<SceneManager>
	{
		friend Singleton<SceneManager>;
	public:
		/**
		 * Sceneの追加
		 * @param[in] _pScene 追加するSceneのポインタ
		 */
		void AddScene(const SharedPtr<SceneBase>& _pScene);

		/**
		 * Sceneの削除
		 * @param[in] _sceneName 削除するSceneの名前
		 */
		void RemoveScene(std::string _sceneName);

		void AllRemoveScene()
		{
			m_pScenes.clear();
		}

		/**
		 * Sceneの変更
		 * @param[in] _sceneName 変更先のSceneの名前
		 */
		void ChangeScene(std::string _sceneName);

		/**
		 * ゲームを終了させる
		 */
		void GameEnd()
		{
			m_IsGameEnd = true;
		}

		/**
		 * SceneManagerの実行
		 * @return 終了するときにtrueになる
		 */
		bool Run();

	private:
		SceneManager();
		~SceneManager();


		/**
		 * SceneManagerの状態
		 */
		enum State
		{
			SCENE_CREATE,	//!< シーンの生成状態
			SCENE_PROC,		//!< シーンの処理状態		
			SCENE_RELEASE	//!< シーンの解放状態
		};


		State							  m_State;
		azk::SharedPtr<SceneBase>		  m_pCurrentScene; //!< 現在のシーン
		std::string						  m_CurrentSceneName;
		std::map<std::string, azk::SharedPtr<SceneBase>> m_pScenes;
		bool							  m_IsGameEnd;
	};
}


#endif
