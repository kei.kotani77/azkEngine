#ifndef AZK_TASK_HOLDER_H
#define AZK_TASK_HOLDER_H

namespace azk
{
	class ObjectBase
	{
	public:
		ObjectBase() {}
		virtual ~ObjectBase() {}

		virtual bool Initialize() = 0;
		virtual void Finalize() = 0;

	};

	class TaskHolder
	{
	public:
		template<class T, class Func>
		TaskHolder(T* _pInstance, Func _func)
			: m_pInstance(_pInstance)
			, pFunc(static_cast<void(ObjectBase::*)()>(_func)){}

		TaskHolder() {}

		virtual ~TaskHolder() {}

		void Run()
		{
			(m_pInstance->*pFunc)();
		}

		struct TaskCmp
		{
			bool operator()(const TaskHolder& _task1, TaskHolder& _task2) const
			{
				return (_task1.m_Priority < _task2.m_Priority);
			}
			bool operator()(TaskHolder* _task1, TaskHolder* _task2) const
			{
				return (_task1->m_Priority < _task2->m_Priority);
			}
		};


	private:
		void(ObjectBase::*pFunc)();

		ObjectBase* m_pInstance;
		int			m_Priority;
		
	};
}


#endif
