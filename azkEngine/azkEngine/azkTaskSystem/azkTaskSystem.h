#ifndef AZK_TASK_SYSTEM_H
#define AZK_TASK_SYSTEM_H
#include <list>
#include <unordered_map>

#include "..\Singleton\Singleton.h"
#include "azkTaskHolder/azkTaskHolder.h"
#include "..\DebugTool\ScopeTimer.h"

namespace azk
{
	class TaskSystem : public Singleton<TaskSystem>
	{
		friend azk::Singleton<TaskSystem>;
	public:
		TaskSystem() {}
		~TaskSystem() {}

		void AddTask(int _group , TaskHolder* _pTask)
		{
			m_pTasks[_group].push_back(_pTask);
		}

		void RemoveTask(int _group, TaskHolder* _pTask)
		{
			TaskList* taskList = &m_pTasks[_group];
		}

		void Run(int _group)
		{
			m_pTasks[_group].sort(TaskHolder::TaskCmp());
			for (const auto& itr : m_pTasks[_group])
				itr->Run();
		}

		void Clear()
		{
			for (auto& itr : m_pTasks)
				itr.second.clear();

			m_pTasks.clear();
		}

	private:
		using TaskList = std::list<TaskHolder*>;
		std::unordered_map<int, TaskList> m_pTasks;
		
	};
}


#endif
