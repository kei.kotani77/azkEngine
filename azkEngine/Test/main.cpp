#include "azkApplication/azkApplicationBase.h"
#include "Application/Application.h"
#include "SmartPointer/UniquePtr.h"
#include "SmartPointer/SharedPtr.h"

#include "azkTaskSystem/azkTaskHolder/azkTaskHolder.h"


ENTRY_POINT()
{
	azk::SystemSetting setting;
	setting.WindowWidth = 1270;
	setting.WindowHeight = 720;
	azk::UniquePtr<Application> pTest = new Application();

	return pTest->Boot(setting,"Test");
}