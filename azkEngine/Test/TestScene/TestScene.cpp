#include "TestScene.h"
#include "ObjectManager/ObjectManager.h"
#include "azkTaskSystem/azkTaskSystem.h"


TestScene::TestScene() :
	azk::SceneBase("TestScene")
{
	SINGLETON_CREATE(ObjectManager);
}


TestScene::~TestScene()
{
	SINGLETON_DELETE(ObjectManager);
}

bool TestScene::Initialize()
{
	return true;
}

void TestScene::Finalize()
{
}


void TestScene::Run()
{
	SINGLETON_INSTANCE(azk::TaskSystem).Run(0);;
	SINGLETON_INSTANCE(azk::TaskSystem).Run(1);;
}

