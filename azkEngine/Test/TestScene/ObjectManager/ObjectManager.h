#ifndef OBJECT_MANAGER_H
#define OBJECT_MANAGER_H
#include "Singleton/Singleton.h"
#include "azkTaskSystem/azkTaskHolder/azkTaskHolder.h"
#include <vector>

class ObjectManager : public azk::Singleton<ObjectManager>
{
	friend azk::Singleton<ObjectManager>;
public:
	ObjectManager();
	~ObjectManager();

	bool Initialize() { return true; }

	void Finalize() {}

private:
	std::vector<azk::ObjectBase*> m_pObjects;

};


#endif
