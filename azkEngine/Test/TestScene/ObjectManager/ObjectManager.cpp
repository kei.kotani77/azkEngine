#include "ObjectManager.h"
#include "azkTaskSystem/azkTaskHolder/azkTaskHolder.h"
#include "TestObject/TestObject.h"
#include "Utility/Utility.h"


ObjectManager::ObjectManager()
{
	m_pObjects.push_back(new TestObject());
}


ObjectManager::~ObjectManager()
{
	for (int i = 0; i < m_pObjects.size(); i++)
	{
		m_pObjects[i]->Finalize();
		azk::SafeDelete(m_pObjects[i]);
	}
}
