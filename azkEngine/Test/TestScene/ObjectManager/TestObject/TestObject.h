#ifndef TEST_OBJECT_H
#define TEST_OBJECT_H
#include "azkTaskSystem/azkTaskHolder/azkTaskHolder.h"

class TestObject : public azk::ObjectBase
{
public:
	TestObject();
	~TestObject();

	bool Initialize() override { return true; }

	void Finalize() override {}

private:
	void Update() 
	{
	}
	void Draw() {}

	azk::TaskHolder* m_pUpdate;
	azk::TaskHolder* m_pDraw;

};


#endif
