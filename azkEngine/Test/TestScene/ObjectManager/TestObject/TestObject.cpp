#include "TestObject.h"
#include "azkTaskSystem/azkTaskSystem.h"
#include "Utility/Utility.h"

TestObject::TestObject()
{
	m_pUpdate = new azk::TaskHolder(this, &TestObject::Update);
	m_pDraw = new azk::TaskHolder(this, &TestObject::Draw);
	SINGLETON_INSTANCE(azk::TaskSystem).AddTask(0, m_pUpdate);
	SINGLETON_INSTANCE(azk::TaskSystem).AddTask(1, m_pDraw);
}


TestObject::~TestObject()
{
	azk::SafeDelete(m_pUpdate);
	azk::SafeDelete(m_pDraw);
}
