#ifndef TESTSCENE_H
#define TESTSCENE_H
#include "azkSceneManager/SceneBase/SceneBase.h"

class TestScene : public azk::SceneBase
{
public:
	TestScene();
	~TestScene();

	bool Initialize() override;

	void Finalize() override;

	void Run() override;

};


#endif
