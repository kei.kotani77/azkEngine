#ifndef APPLICATION_H
#define APPLICATION_H
#include "azkApplication/azkApplicationBase.h"

class Application : public azk::ApplicationBase
{
public:
	Application() {};

	~Application() { Finalize(); }

	bool Initialize() override;

	void Finalize() override;


};


#endif
