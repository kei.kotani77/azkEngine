#include "Application.h"
#include "azkTaskSystem/azkTaskSystem.h"
#include "azkSceneManager/azkSceneManager.h"
#include "..\TestScene\TestScene.h"


bool Application::Initialize()
{	
	SINGLETON_CREATE(azk::TaskSystem);
	SINGLETON_INSTANCE(azk::SceneManager).AddScene(new TestScene());
	SINGLETON_INSTANCE(azk::SceneManager).ChangeScene("TestScene");

	return true;
}

void Application::Finalize()
{
	SINGLETON_DELETE(azk::TaskSystem);
}